package com.tiendaC3.misionTIC;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TiendaC3Application {

	public static void main(String[] args) {
		SpringApplication.run(TiendaC3Application.class, args);
	}

}
